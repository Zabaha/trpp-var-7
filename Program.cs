﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace Парсер
{
    class Program
    {
        static void DownloadPhoto(WebClient wc)
        {
            string path = "https://student.mirea.ru/media/photo/";
            string forPhotoLoad = "https://student.mirea.ru/";
            wc.Encoding = System.Text.Encoding.UTF8;
            string dataForAlbuns = wc.DownloadString(path); ;
            MatchCollection albumLinks = getAlbumLinks(dataForAlbuns);
            for (int i = 0; i < 6; i++)
            {
                string dataForPhoto = wc.DownloadString(path + albumLinks[i].Groups[1].Value);
                string nameOfAlbum = getAlbumName(dataForPhoto);
                MatchCollection nameOfPhoto = getPhotoName(dataForPhoto);
                MatchCollection photoLinks = getPhotoLinks(dataForPhoto);
                int photoCounter = photoLinks.Count;
                Console.WriteLine("Создание папки " + nameOfAlbum);
                Directory.CreateDirectory(@"C:\Users\dexte\Documents\практика\Парсер\Download\" + nameOfAlbum);
                Console.WriteLine("Изменение директории на " + nameOfAlbum);
                for (int j = 0; j < photoCounter; j++)
                {
                    try
                    {
                        wc.DownloadFile(forPhotoLoad + photoLinks[j].Groups[1].Value, @"C:\Users\dexte\Documents\практика\Парсер\Download\" + nameOfAlbum + @"\" + nameOfPhoto[j].Groups[2].Value + ".jpg");
                        Console.WriteLine("файл " + nameOfPhoto[j].Groups[2].Value + " загружен");
                    }
                    catch
                    {
                        Console.WriteLine("ошибка при загрузке файла " + nameOfPhoto[j].Groups[2].Value);
                    }
                }
                Console.WriteLine("Выход из директории");
            }
        }

        static MatchCollection getAlbumLinks(string dataForPhoto)
        {
            MatchCollection macthes = Regex.Matches(dataForPhoto, @"<a class=""u-link-v2"" href=""/media/photo/(.*?)""></a>");
            return macthes;
        }

        static MatchCollection getPhotoLinks(string dataForPhoto)
        {
            MatchCollection macthes = Regex.Matches(dataForPhoto, @"<img class=""img-fluid u-block-hover__main--grayscale u-block-hover__img"" src=""(/upload/iblock/.*?/.*?.jpg)"">");
            return macthes;
        }

        static string getAlbumName(string dataForPhoto)
        {
            Match macth = Regex.Match(dataForPhoto, @"<h2 class=""text-uppercase u-heading-v2__title g-mb-5"">(.*?)</h2>");
            return macth.Groups[1].Value;
        }

        static MatchCollection getPhotoName(string dataForPhoto)
        {
            MatchCollection macthes = Regex.Matches(dataForPhoto, @"<img class=""img-fluid u-block-hover__main--grayscale u-block-hover__img"" src=""/upload/iblock/(.*?)/(.*?).jpg"">");
            return macthes;
        }

        static void Main(string[] args)
        {
            WebClient wc = new WebClient();
            DownloadPhoto(wc);
            Console.ReadLine();
        }
    }
}
